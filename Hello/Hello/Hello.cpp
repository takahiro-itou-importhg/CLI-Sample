// Hello.cpp : メイン プロジェクト ファイルです。

#include "stdafx.h"
#include    <iostream>
#using      <mscorlib.dll>

int main(array<System::String ^> ^args)
{
    System::String^ fileIn  = "sample.txt";
    System::String^ fileOut = "output.csv";
    
    System::IO::StreamReader^   sr;
    System::IO::StreamWriter^   sw;
    
    try {
        sr  =  System::IO::File::OpenText(fileIn);
        sw  =  gcnew  System::IO::StreamWriter(fileOut);

        System::String^ strLine;
        int cnt = 0;
        
        while ( (strLine = sr->ReadLine()) != nullptr )
        {
            ++ cnt;
            sw->WriteLine(cnt + ":"  + strLine);
        }
    } catch ( System::IO::FileNotFoundException^ e ) {
        System::Console::WriteLine("File {0} Not Found.",  fileIn);
    } catch ( System::Exception^ e ) {
        System::Console::WriteLine("Error.");
    } finally {
        sw->Close();
        sr->Close();
    }

    return 0;
}
